# YouTrack template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[YouTrack](https://www.jetbrains.com/youtrack/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of YouTrack (see
[Bastillefile](Bastillefile)) will be installed using default settings.

**Please note that you have to enable Linux support for it to run correctly!**

YouTrack will be configured to listen on `BASE_URL` and port `PORT` for
connections and the JVM will need 4 GB of RAM.

## Deprecation of JAR distribution

Since the deprecation of the JAR distribution of YouTrack setup is now more
complex and takes more time. Also this template template is no longer working
for older versions (i.e. JAR distributions) of YouTrack.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

```
# bastille bootstrap https://codeberg.org/wegtam/bastille-youtrack
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-youtrack --arg EXT_DIR=/srv/youtrack-data
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-youtrack --arg EXT_DIR=/foo \
    --arg VERSION=2023.3.24329 \
    --arg BASE_URL=https://my.reverse.proxy/youtrack \
    --arg PORT=8081
```

For more options take a look at the [Bastillefile](Bastillefile).

